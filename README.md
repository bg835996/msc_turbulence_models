## Turbulence Modelling Approaches for the Atmospheric Boundary Layer

MSc project

Department of Meteorology, University of Reading, UK

Somrath Kanoksirirath

* main.py = Run and plot
* IC.py = Class for initial conditions
* BC.py = Class for boundary conditions
* abstractModels.py = Abstract classes for turbulence models
* grid.py = 1D Staggered grid class
* FirstOrderModels.py = Several first-order turbulence models/classes
* KLModels.py = Several k-l turbulence models/classes
* KEModels.py = Several k-eps turbulence models/classes
* tools.py = functions for plotting graphs
* plotPr.py = compare Pr of the implemented (first-order) models

Note: Stability = restrict because of explicit schemes, 
but will be discard as results appear independent of dt

